package com.qh.learnenglish.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.qh.learnenglish.control.QuestionControl;
import com.qh.learnenglish.model.Lesson;
import com.qh.learnenglish.model.Question;

import java.util.HashMap;

/**
 * Created by osx on 3/9/17.
 */
public class LessonAdapter extends BaseAdapter implements QuestionControl.QuestionCheckListener {

    private Context context;
    private Lesson lesson;
    private boolean reviewMode;
    private int[] selection;

    public LessonAdapter(Context context, Lesson lesson){
        this.context = context;
        this.lesson = lesson;
        this.selection = new int[lesson.size()];

        for(int i=0;i<selection.length;i++){
            selection[i] = -1;
        }
    }

    public int getCorrectCount(){
        int count = 0;
        for(int i=0;i<lesson.size();i++){
            if(selection[i] == lesson.get(i).getCorrect()){
                count++;
            }
        }
        return count;
    }

    @Override
    public int getCount() {
        return lesson.size();
    }

    @Override
    public Object getItem(int position) {
        return lesson.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        QuestionControl questionControl = null;
        Question question = lesson.get(position);

        if(convertView ==null){
            questionControl = new QuestionControl(this.context);
            questionControl.setQuestionCheckListener(this);
        }
        else{
            questionControl = (QuestionControl)convertView;
        }

        questionControl.setQuestion(question, position, selection[position]);
        questionControl.setReviewMode(reviewMode);

        return questionControl;
    }

    @Override
    public void onCheck(Question question, int newIndex) {
        selection[lesson.indexOf(question)] = newIndex;
    }

    public void setReviewMode(boolean reviewMode) {
        this.reviewMode = reviewMode;
        notifyDataSetChanged();
    }
}
