package com.qh.learnenglish.adapter;

import android.content.Context;
import android.graphics.Shader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.qh.learnenglish.R;
import com.qh.learnenglish.model.VocabularyCategory;
import com.qh.learnenglish.model.VocabularyCategoryList;
import com.qh.learnenglish.utils.UIUtils;

/**
 * Created by osx on 4/20/17.
 */
public class CategoryAdapter extends BaseAdapter {

    private VocabularyCategoryList vocabularyCategories;

    public CategoryAdapter(VocabularyCategoryList vocabularyCategories){
        this.vocabularyCategories = vocabularyCategories;
    }

    @Override
    public int getCount() {
        return vocabularyCategories.size();
    }

    @Override
    public Object getItem(int position) {
        return vocabularyCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.category_item_layout, parent, false);
        }

        RoundedImageView imageView = (RoundedImageView)convertView.findViewById(R.id.img_view);
        TextView textView = (TextView)convertView.findViewById(R.id.txt_category);
        VocabularyCategory category = this.vocabularyCategories.get(position);

        textView.setText(category.getDisplay());
        imageView.setImageDrawable(UIUtils.createDrawable(context, "data/" + category.getFolder()+ "/icon.png"));

        return convertView;
    }
}
