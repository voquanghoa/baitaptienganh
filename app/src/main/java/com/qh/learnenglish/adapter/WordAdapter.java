package com.qh.learnenglish.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.qh.learnenglish.R;
import com.qh.learnenglish.model.VocabularyStore;
import com.qh.learnenglish.model.VocabularyStoreData;
import com.qh.learnenglish.utils.UIUtils;

/**
 * Created by osx on 4/20/17.
 */
public class WordAdapter extends BaseAdapter{

    private VocabularyStoreData categoryWords;

    public WordAdapter(VocabularyStoreData categoryWords){
        this.categoryWords = categoryWords;
    }

    @Override
    public int getCount() {
        return this.categoryWords!=null?this.categoryWords.size():0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.category_item_layout, parent, false);
        }

        RoundedImageView imageView = (RoundedImageView)convertView.findViewById(R.id.img_view);
        TextView textView = (TextView)convertView.findViewById(R.id.txt_category);

        VocabularyStore vocabularyStore = this.categoryWords.getVocabularyStores().get(position);

        String path = "data/" + vocabularyStore.getCategory()+ "/picture/" + vocabularyStore.getEnglish() + ".png";

        textView.setText(vocabularyStore.getVietnamese());

        imageView.setImageDrawable(UIUtils.createDrawable(context, path));

        return convertView;
    }
}
