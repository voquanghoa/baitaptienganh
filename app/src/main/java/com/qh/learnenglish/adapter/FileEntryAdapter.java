package com.qh.learnenglish.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qh.learnenglish.R;
import com.qh.learnenglish.model.FileEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by osx on 3/8/17.
 */
public class FileEntryAdapter extends BaseAdapter {

    private List<FileEntry> files = new ArrayList<>();
    private Context context;

    public FileEntryAdapter(Context context){
        this.context = context;
    }


    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public Object getItem(int position) {
        return files.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<FileEntry> files){
        this.files.clear();

        if(files != null){
            this.files.addAll(files);
        }

        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FileEntry fileEntry = files.get(position);

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view_layout, parent, false);
        }

        TextView textView = (TextView)convertView.findViewById(R.id.label);
        textView.setText(fileEntry.getDisplay());
        convertView.setTag(fileEntry);

        return convertView;
    }
}
