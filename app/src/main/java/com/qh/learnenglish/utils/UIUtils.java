package com.qh.learnenglish.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.io.IOException;

/**
 * Created by osx on 4/20/17.
 */
public class UIUtils {
    public static Drawable createDrawable(Context context, String fileName){
        try {
            return Drawable.createFromStream(context.getAssets().open(fileName), null);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
