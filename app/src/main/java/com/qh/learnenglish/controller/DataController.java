package com.qh.learnenglish.controller;

import android.content.Context;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by osx on 3/8/17.
 */
public class DataController {

    private DataController(Context context){
        this.context = context;
    }
    private Context context;

    public static DataController from(Context context){
        return new DataController(context);
    }

    public boolean isFileExist(String filePath){
        try {
            InputStream steam = context.getAssets().open(filePath);
            steam.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public <T> T readJson(String path, Class<T> tClass) throws IOException {
        String fileData = readFile(path);
        return new Gson().fromJson(fileData, tClass);
    }

    public String readFile(String path) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(path)));
            StringBuffer stringBuffer = new StringBuffer();

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                stringBuffer.append(mLine+"\n");
            }
            return stringBuffer.toString();
        } finally {
            if(reader!=null) {
                reader.close();
            }
        }
    }
}
