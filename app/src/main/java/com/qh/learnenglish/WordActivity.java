package com.qh.learnenglish;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.qh.learnenglish.control.WordLearningControl;
import com.qh.learnenglish.controller.DataController;
import com.qh.learnenglish.model.VocabularyCategory;
import com.qh.learnenglish.model.VocabularyCategoryList;

import java.io.IOException;

/**
 * Created by osx on 4/18/17.
 */
public class WordActivity extends BaseActivity {

    private VocabularyCategoryList vocabularyCategoryList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_learning_activity_layout);

        WordLearningControl wordLearningControl = (WordLearningControl)findViewById(R.id.main_control);
        try {
            vocabularyCategoryList = DataController.from(this).readJson("data/list.json", VocabularyCategoryList.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        VocabularyCategory vocabularyCategory = vocabularyCategoryList.getByFolder(getIntent().getStringExtra("category"));

        wordLearningControl.setVocabularyCategory(vocabularyCategory);
        wordLearningControl.setWord(getIntent().getStringExtra("file"));
        setTitle(vocabularyCategory.getDisplay());
    }
}
