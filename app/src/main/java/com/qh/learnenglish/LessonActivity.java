package com.qh.learnenglish;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;

import com.google.gson.Gson;
import com.qh.learnenglish.controller.DataController;
import com.qh.learnenglish.model.FileEntry;

import java.io.IOException;

/**
 * Created by Dell on 5/6/2017.
 */

public class LessonActivity extends BaseActivity {

    private String fileName;
    private String display;
    private FileEntry fileEntry;
    private WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_activity_layout);

        fileName = getIntent().getStringExtra("file_path");
        display = getIntent().getStringExtra("file_name");
        String json = getIntent().getStringExtra("folder");

        fileEntry = new Gson().fromJson(json, FileEntry.class);

        setTitle(display);
        webView = (WebView)findViewById(R.id.webView);
        try {
            webView.loadData(DataController.from(this).readFile(fileName+"/lesson.html"), "text/html; charset=UTF-8",null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doExercise(View view){
        new AlertDialog.Builder(this)
                .setItems(fileEntry.getChildrenDisplays(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LessonActivity.this, ExerciseActivity.class);
                        FileEntry entry = fileEntry.getChildren().get(which);
                        intent.putExtra("file_path", fileName + "/" + entry.getFile());
                        intent.putExtra("file_name", entry.getDisplay());

                        startActivity(intent);
                    }
                })
                .setTitle("Chọn bài tập")
                .show();
    }
}
