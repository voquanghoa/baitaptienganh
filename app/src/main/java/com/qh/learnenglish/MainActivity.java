package com.qh.learnenglish;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.qh.learnenglish.controller.DataController;
import com.qh.learnenglish.fragment.BackeyHandler;
import com.qh.learnenglish.fragment.CategoryFragment;
import com.qh.learnenglish.fragment.SearchFragment;
import com.qh.learnenglish.fragment.FavoritesFragment;
import com.qh.learnenglish.fragment.TestMenuFragment;
import com.qh.learnenglish.model.FileEntry;
import com.qh.learnenglish.model.VocabularyCategoryList;

import java.io.IOException;

/**
 * Created by osx on 4/19/17.
 */
public class MainActivity extends AppCompatActivity {

    private VocabularyCategoryList vocabularyCategoryList;
    private FileEntry rootFileEntry;
    private Fragment currentFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            vocabularyCategoryList = DataController.from(this).readJson("data/list.json", VocabularyCategoryList.class);
            rootFileEntry = DataController.from(this).readJson("grammar/data.json", FileEntry.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        setTitle(item.getTitle());
                        switch (item.getItemId()) {
                            case R.id.action_category:
                                PushFragment(CategoryFragment.newInstance(vocabularyCategoryList));
                                break;
                            case R.id.action_favorites:
                                PushFragment(FavoritesFragment.newInstance());
                                break;
                            case R.id.action_search:
                                PushFragment(SearchFragment.newInstance(vocabularyCategoryList));
                                break;
                            case R.id.action_test:
                                PushFragment(TestMenuFragment.newInstance(rootFileEntry));
                        }
                        return true;
                    }
                });

        PushFragment(CategoryFragment.newInstance(vocabularyCategoryList));
    }

    @Override
    public void onBackPressed() {

        if(currentFragment instanceof BackeyHandler){
            ((BackeyHandler)currentFragment).onBackeyPress();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void PushFragment(Fragment fragment){
        currentFragment = fragment;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, fragment);
        transaction.commit();
    }
}