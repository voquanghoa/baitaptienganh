package com.qh.learnenglish.control;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qh.learnenglish.R;
import com.qh.learnenglish.model.Vocabulary;
import com.qh.learnenglish.model.VocabularyCategory;
import com.qh.learnenglish.model.VocabularyStoreData;

import java.io.IOException;

/**
 * Created by osx on 4/18/17.
 */
public class WordLearningControl extends RelativeLayout implements View.OnClickListener {

    private final String favoriteKey = "favorites";
    private VocabularyCategory vocabularyCategory;

    private ImageView imageView;
    private int currentIndex;
    private Vocabulary currentVocabulary;
    private EffectImageView starButton;
    private TextView textViewTop;
    private TextView textViewBottom;
    private TextView textViewIndex;

    private VocabularyStoreData favorites;

    private SharedPreferences sharedPref;

    public WordLearningControl(Context context) {
        super(context);
        initView();
    }

    public WordLearningControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public WordLearningControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView(){

        sharedPref = getContext().getApplicationContext().getSharedPreferences("favorite", Context.MODE_PRIVATE);
        favorites = new Gson().fromJson(sharedPref.getString(favoriteKey, "{}"), VocabularyStoreData.class);

        LayoutInflater.from(getContext()).inflate(R.layout.word_learning_control_layout, this, true);
        this.imageView = (ImageView)findViewById(R.id.img_view);
        findViewById(R.id.bt_previous).setOnClickListener(this);
        findViewById(R.id.bt_next).setOnClickListener(this);
        findViewById(R.id.bt_audio).setOnClickListener(this);

        textViewTop = (TextView)findViewById(R.id.text_top);
        textViewBottom = (TextView)findViewById(R.id.text_bottom);
        textViewIndex = (TextView)findViewById(R.id.text_index);
        starButton = (EffectImageView)findViewById(R.id.star_button);

        starButton.setOnClickListener(this);
    }

    public void setVocabularyCategory(VocabularyCategory vocabularyCategory) {
        this.vocabularyCategory = vocabularyCategory;
        setWord(0);
    }

    public void setWord(int index){

        if(index >= 0 && index<vocabularyCategory.getVocabularies().size()){

            this.currentIndex = index;
            currentVocabulary = this.vocabularyCategory.getVocabularies().get(index);

            this.imageView.setImageDrawable(createDrawable(getFileName("picture")));
            textViewTop.setText(currentVocabulary.getVietnamese());
            textViewBottom.setText(currentVocabulary.getLaos());
            textViewIndex.setText(String.format("%d/%d", currentIndex + 1, vocabularyCategory.getVocabularies().size()));

            starButton.setActivated(favorites.contains(vocabularyCategory.getFolder(), currentVocabulary.getEnglish()));
        }
    }

    public void setWord(Vocabulary vocabulary){
        if(vocabulary != null){
            setWord(this.vocabularyCategory.getVocabularies().indexOf(vocabulary));
        }
    }

    public void setWord(String vocabulary){
        setWord(this.vocabularyCategory.findVocabulary(vocabulary));
    }

    private String getFileName(String type){
        String prefix = "data/" + this.vocabularyCategory.getFolder() + "/" + type + "/";

        if(type.equals("picture")){
            return prefix + currentVocabulary.getEnglish() + ".png";
        }

        if(type.equals("audio")){
            return prefix + currentVocabulary.getEnglish() + ".mp3";
        }

        return "";
    }

    private void playAudio(){
        try {
            String file = getFileName("audio");
            AssetFileDescriptor descriptor = getContext().getAssets().openFd(file);

            MediaPlayer player = new MediaPlayer();

            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();

            player.setDataSource(descriptor.getFileDescriptor(), start, end);
            player.prepare();

            player.setVolume(1.0f, 1.0f);
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Drawable createDrawable(String fileName){
        try {
            return Drawable.createFromStream(getContext().getAssets().open(fileName), null);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void toggleStar(){
        SharedPreferences.Editor editor = sharedPref.edit();
        starButton.setActivated(!starButton.isActivated());

        if(starButton.isActivated()){
            favorites.add(vocabularyCategory.getFolder(), currentVocabulary);
        }else{
            favorites.remove(vocabularyCategory.getFolder(), currentVocabulary);
        }

        editor.putString(favoriteKey, new Gson().toJson(favorites));
        editor.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_next:
                setWord(currentIndex + 1);
                break;
            case R.id.bt_previous:
                setWord(currentIndex - 1);
                break;
            case R.id.bt_audio:
                playAudio();
                break;
            case R.id.star_button:
                toggleStar();
                break;
        }
    }
}
