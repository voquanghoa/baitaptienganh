package com.qh.learnenglish.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by osx on 3/31/17.
 */
public class EffectImageView extends ImageView {

    private int effectColor = 0x77eeddff;

    public EffectImageView(Context context) {
        super(context);
        initView();
    }

    public EffectImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public EffectImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    protected void initView(){
        setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        EffectImageView.this.setColorFilter(effectColor, PorterDuff.Mode.SRC_ATOP);
                        EffectImageView.this.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                        performClickIfValid(event);
                    case MotionEvent.ACTION_CANCEL: {
                        EffectImageView.this.clearColorFilter();
                        EffectImageView.this.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
    }

    private void performClickIfValid(MotionEvent event){
        if(isValueBetween(event.getX(), 0, this.getWidth()) &&
                isValueBetween(event.getY(), 0, this.getHeight())){
            try {
                this.performClick();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private boolean isValueBetween(double value, double min, double max){
        return value>=min && value<=max;
    }
}
