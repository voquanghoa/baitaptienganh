package com.qh.learnenglish.control;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.qh.learnenglish.R;
import com.qh.learnenglish.model.Lesson;
import com.qh.learnenglish.model.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by osx on 3/9/17.
 */
public class QuestionControl extends LinearLayout implements AnswerRadioButton.OnCheckedChangeListener {

    private QuestionCheckListener questionCheckListener;
    private List<AnswerRadioButton> radioButtonList;
    private ImageView imgViewCorrect;
    private Question question;
    private TextView textView;

    public void setQuestionCheckListener(QuestionCheckListener questionCheckListener) {
        this.questionCheckListener = questionCheckListener;
    }

    public void setReviewMode(boolean reviewMode) {

        if(reviewMode && isCorrect()){
            imgViewCorrect.setVisibility(VISIBLE);
        }else{
            imgViewCorrect.setVisibility(GONE);
        }

        for(int i=0;i< radioButtonList.size();i++){
            AnswerRadioButton radioButton = radioButtonList.get(i);
            radioButton.setEnabled(!reviewMode);
            radioButton.setTextColor(getResources().getColor(R.color.colorTextNormal));

            if(reviewMode){
                radioButton.setIsCorrect(question.getCorrect() == i);
                radioButton.updateTextStyle();
                radioButton.updateIcon();
            }
        }
    }

    @Override
    public void onCheckedChanged(AnswerRadioButton buttonView, boolean isChecked) {
        if(questionCheckListener != null && isChecked){
            int index = Integer.parseInt(buttonView.getTag().toString());

            questionCheckListener.onCheck(question, index);
        }
    }

    public interface QuestionCheckListener{
        void onCheck(Question lesson, int newIndex);
    }

    public QuestionControl(Context context) {
        super(context);
        initView();
    }

    public QuestionControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public QuestionControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView(){
        LayoutInflater.from(getContext()).inflate(R.layout.lesson_layout, this, true);
        radioButtonList = new ArrayList<>();
        imgViewCorrect = (ImageView)findViewById(R.id.icon_correct);
        textView = (TextView)findViewById(R.id.question);

        addRadioButton(R.id.answer_1);
        addRadioButton(R.id.answer_2);
        addRadioButton(R.id.answer_3);
        addRadioButton(R.id.answer_4);

        this.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addRadioButton(@IdRes int radioId){
        AnswerRadioButton radioButton = (AnswerRadioButton)this.findViewById(radioId);

        radioButton.setOnCheckedChangeListener(this);
        radioButtonList.add(radioButton);
    }

    public void setQuestion(Question question, int index, int current){
        this.question = question;

        for(int i=0;i<radioButtonList.size();i++){
            AnswerRadioButton radioButton = radioButtonList.get(i);

            if(i<question.getAnswers().size()){
                radioButton.setText(Html.fromHtml(question.getAnswers().get(i)));
                radioButton.setVisibility(VISIBLE);
            }else{
                radioButton.setVisibility(GONE);
            }

            radioButton.setChecked(i == current);
        }
        textView.setText(Html.fromHtml(String.format(getContext().getString(R.string.question_with_index_format), index + 1, question.getQuestion())));
    }

    public boolean isCorrect(){
        return question.getCorrect()<radioButtonList.size() && radioButtonList.get(question.getCorrect()).isChecked();
    }
}
