
package com.qh.learnenglish.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.qh.learnenglish.R;
import com.qh.learnenglish.WordActivity;
import com.qh.learnenglish.adapter.CategoryAdapter;
import com.qh.learnenglish.model.VocabularyCategory;
import com.qh.learnenglish.model.VocabularyCategoryList;

public class CategoryFragment extends Fragment implements AdapterView.OnItemClickListener {

    private VocabularyCategoryList vocabularyCategories;


    public static CategoryFragment newInstance(VocabularyCategoryList vocabularyCategories) {
        CategoryFragment fragment = new CategoryFragment();
        fragment.vocabularyCategories = vocabularyCategories;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_list_layout, container, false);

        ListView listView = (ListView)view.findViewById(R.id.list_item);
        listView.setAdapter(new CategoryAdapter(vocabularyCategories));
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        VocabularyCategory category = vocabularyCategories.get(position);
        Intent intent = new Intent(this.getContext(), WordActivity.class);

        intent.putExtra("category", category.getFolder());

        this.getActivity().startActivity(intent);

    }
}
