/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.qh.learnenglish.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.qh.learnenglish.R;
import com.qh.learnenglish.WordActivity;
import com.qh.learnenglish.adapter.WordAdapter;
import com.qh.learnenglish.model.VocabularyStore;
import com.qh.learnenglish.model.VocabularyStoreData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FavoritesFragment extends Fragment implements AdapterView.OnItemClickListener {
    private final String favoriteKey = "favorites";

    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        return fragment;
    }

    private VocabularyStoreData favorites;

    private SharedPreferences sharedPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = getActivity().getApplicationContext().getSharedPreferences("favorite", Context.MODE_PRIVATE);
        favorites = new Gson().fromJson(sharedPref.getString(favoriteKey, "{}"), VocabularyStoreData.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_favorites_layout, container, false);
        ListView listView = (ListView)view.findViewById(R.id.list_item);
        listView.setAdapter(new WordAdapter(favorites));
        listView.setOnItemClickListener(this);

        if(favorites.size() > 0){
            view.findViewById(R.id.txt_favorites_empty).setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        VocabularyStore vocabularyStore = favorites.getVocabularyStores().get(position);
        Intent intent = new Intent(this.getContext(), WordActivity.class);

        intent.putExtra("category", vocabularyStore.getCategory());
        intent.putExtra("file", vocabularyStore.getEnglish());

        this.getActivity().startActivity(intent);
    }
}
