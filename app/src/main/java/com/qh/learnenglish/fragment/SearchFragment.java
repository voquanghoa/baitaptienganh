
package com.qh.learnenglish.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.qh.learnenglish.R;
import com.qh.learnenglish.WordActivity;
import com.qh.learnenglish.adapter.WordAdapter;
import com.qh.learnenglish.model.Vocabulary;
import com.qh.learnenglish.model.VocabularyCategory;
import com.qh.learnenglish.model.VocabularyCategoryList;
import com.qh.learnenglish.model.VocabularyStore;
import com.qh.learnenglish.model.VocabularyStoreData;

public class SearchFragment extends Fragment implements AdapterView.OnItemClickListener, TextWatcher {

    private VocabularyCategoryList vocabularyCategories;
    private VocabularyStoreData favorites;
    private WordAdapter wordAdapter;
    private EditText editText;
    private TextView textViewMessage;

    public static SearchFragment newInstance(VocabularyCategoryList vocabularyCategories) {
        SearchFragment fragment = new SearchFragment();
        fragment.vocabularyCategories = vocabularyCategories;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_word_layout, container, false);

        ListView listView = (ListView)view.findViewById(R.id.list_item);
        listView.setAdapter(wordAdapter = new WordAdapter(favorites = new VocabularyStoreData()));
        listView.setOnItemClickListener(this);

        (editText = (EditText)view.findViewById(R.id.inputSearch)).addTextChangedListener(this);
        textViewMessage = (TextView)view.findViewById(R.id.txt_search_message);
        textViewMessage.setVisibility(View.INVISIBLE);
        if(favorites.size() > 0){
            view.findViewById(R.id.txt_favorites_empty).setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        VocabularyStore vocabularyStore = favorites.getVocabularyStores().get(position);
        Intent intent = new Intent(this.getContext(), WordActivity.class);

        intent.putExtra("category", vocabularyStore.getCategory());
        intent.putExtra("file", vocabularyStore.getEnglish());

        this.getActivity().startActivity(intent);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    private boolean isMatch(String str, String pattern){
        if(str == null || pattern == null){
            return false;
        }

        if(str.trim().length() == 0 || pattern.trim().length() == 0){
            return false;
        }

        return str.trim().toLowerCase().contains(pattern.trim().toLowerCase());
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        favorites.getVocabularyStores().clear();

        String pattern = editText.getText().toString();
        if(pattern == null || pattern.trim() == ""){
            wordAdapter.notifyDataSetChanged();
            textViewMessage.setVisibility(View.INVISIBLE);
            return;
        }

        for(VocabularyCategory category: this.vocabularyCategories){
            for(Vocabulary vocabulary : category.getVocabularies()){
                String text = String.format("%s;%s", vocabulary.getVietnamese(), vocabulary.getLaos());
                if(isMatch(text, pattern)){
                    favorites.add(category.getFolder(), vocabulary);
                }
            }
        }

        wordAdapter.notifyDataSetChanged();
        if(favorites.any()){
            textViewMessage.setVisibility(View.INVISIBLE);
        }else{
            textViewMessage.setVisibility(View.VISIBLE);
        }
    }
}
