package com.qh.learnenglish.fragment;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.qh.learnenglish.ExerciseActivity;
import com.qh.learnenglish.LessonActivity;
import com.qh.learnenglish.R;
import com.qh.learnenglish.adapter.FileEntryAdapter;
import com.qh.learnenglish.controller.DataController;
import com.qh.learnenglish.model.FileEntry;

import java.util.Stack;

/**
 * Created by Dell on 4/26/2017.
 */

public class TestMenuFragment extends Fragment implements AdapterView.OnItemClickListener, BackeyHandler {

    private ListView lvList;

    private FileEntryAdapter fileEntryAdapter;
    private FileEntry rootFileEntry;
    private DataController dataController;

    private Stack<FileEntry> fileEntryStack;

    public static TestMenuFragment newInstance(FileEntry fileEntry){
        TestMenuFragment fragment = new TestMenuFragment();

        fragment.rootFileEntry = fileEntry;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        dataController = DataController.from(inflater.getContext());
        View view = inflater.inflate(R.layout.list_grammar_fragment_layout, container, false);

        lvList = (ListView)view.findViewById(R.id.list_item);

        lvList.setOnItemClickListener(this);

        fileEntryAdapter = new FileEntryAdapter(this.getActivity());
        fileEntryStack = new Stack<>();


        lvList.setAdapter(fileEntryAdapter);

        display(rootFileEntry);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FileEntry fileEntry = (FileEntry)view.getTag();

        String fullPath = getFullPath(fileEntry);

        if(dataController.isFileExist(fullPath + "/lesson.html")){
            Intent intent = new Intent(this.getActivity(), LessonActivity.class);

            intent.putExtra("file_path", fullPath);
            intent.putExtra("file_name", fileEntry.getDisplay());
            intent.putExtra("folder", new Gson().toJson(fileEntry));

            startActivity(intent);
        }else{
            display(fileEntry);
        }
    }

    @NonNull
    private String getFullPath(FileEntry fileEntry) {
        StringBuffer sb = new StringBuffer();

        for(int i=1;i<fileEntryStack.size();i++){
            sb.append(fileEntryStack.get(i).getFile());
            sb.append("/");
        }
        sb.append(fileEntry.getFile());
        return "grammar/" + sb.toString();
    }


    private void display(FileEntry fileEntry){
        fileEntryStack.push(fileEntry);
        fileEntryAdapter.setItems(fileEntry.getChildren());
        updateActionBar(fileEntryStack.size()>1, fileEntry.getDisplay());
    }

    private void updateActionBar(boolean displayBack, String title){
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(displayBack);
        getActivity().setTitle(title);
    }

    @Override
    public boolean onBackeyPress() {
        if(fileEntryStack.size()>1){
            fileEntryStack.pop();
            fileEntryAdapter.setItems(fileEntryStack.peek().getChildren());
            updateActionBar(fileEntryStack.size()>1, fileEntryStack.peek().getDisplay());
            return true;
        }
        return false;
    }
}
