package com.qh.learnenglish.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by osx on 4/1/17.
 */
public class WordItem {

    @SerializedName("file_name")
    private String fileName;

    @SerializedName("word")
    private String word;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
