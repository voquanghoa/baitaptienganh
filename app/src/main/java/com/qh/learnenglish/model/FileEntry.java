package com.qh.learnenglish.model;

import java.util.List;

/**
 * Created by osx on 3/8/17.
 */
public class FileEntry {

    private String file;
    private String display;
    private List<FileEntry> children;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public List<FileEntry> getChildren() {
        return children;
    }

    public String[] getChildrenDisplays(){
        String[] array = new String[children.size()];

        for(int i=0;i<children.size();i++){
            array[i] = children.get(i).getDisplay();
        }

        return array;
    }

    public void setChildren(List<FileEntry> children) {
        this.children = children;
    }

    public boolean hasChildren(){
        return children!=null && children.size()>0;
    }
}
