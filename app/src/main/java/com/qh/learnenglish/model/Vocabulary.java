package com.qh.learnenglish.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by osx on 4/19/17.
 */
public class Vocabulary implements Serializable{

    @SerializedName("vocabulary_eng")
    private String english;

    @SerializedName("vocabulary_vn")
    private String vietnamese;

    @SerializedName("vocabulary_lao")
    private String laos;

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getVietnamese() {
        return vietnamese;
    }

    public void setVietnamese(String vietnamese) {
        this.vietnamese = vietnamese;
    }

    public String getLaos() {
        return laos;
    }

    public void setLaos(String laos) {
        this.laos = laos;
    }
}
