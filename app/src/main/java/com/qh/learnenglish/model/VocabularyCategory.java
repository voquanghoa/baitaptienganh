package com.qh.learnenglish.model;

import android.content.Intent;

import com.qh.learnenglish.WordActivity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by osx on 4/19/17.
 */
public class VocabularyCategory implements Serializable {

    private String folder;

    private String display;

    private ArrayList<Vocabulary> vocabularies;

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public ArrayList<Vocabulary> getVocabularies() {
        return vocabularies;
    }

    public void setVocabularies(ArrayList<Vocabulary> vocabularies) {
        this.vocabularies = vocabularies;
    }

    public Vocabulary findVocabulary(String vocabulary){
        for(Vocabulary voc : this.vocabularies){
            if(voc.getEnglish().equals(vocabulary)){
                return voc;
            }
        }
        return null;
    }
}
