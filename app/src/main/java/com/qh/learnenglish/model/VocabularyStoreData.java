package com.qh.learnenglish.model;

import java.util.ArrayList;

/**
 * Created by osx on 4/20/17.
 */
public class VocabularyStoreData {

    private ArrayList<VocabularyStore> vocabularyStores;

    public VocabularyStore find(String category, String word){

        for(VocabularyStore vocabularyStore: getVocabularyStores()){
            if(vocabularyStore.getCategory().equals(category)
                    && vocabularyStore.getEnglish().equals(word)){
                return vocabularyStore;
            }
        }

        return null;
    }

    public ArrayList<VocabularyStore> getVocabularyStores(){

        if(vocabularyStores == null){
            vocabularyStores = new ArrayList<>();
        }
        return vocabularyStores;
    }

    public void add(String category, Vocabulary vocabulary){
        VocabularyStore vocabularyStore = new VocabularyStore();

        vocabularyStore.setCategory(category);
        vocabularyStore.setEnglish(vocabulary.getEnglish());
        vocabularyStore.setLaos(vocabulary.getLaos());
        vocabularyStore.setVietnamese(vocabulary.getVietnamese());

        getVocabularyStores().add(0, vocabularyStore);
    }

    public boolean contains(String category, String word){
        return find(category, word) != null;
    }

    public void remove(String category, String word){
        VocabularyStore vocabularyStore = find(category, word);
        if(vocabularyStore != null){
            getVocabularyStores().remove(vocabularyStore);
        }
    }

    public void remove(String category, Vocabulary vocabulary){
        remove(category, vocabulary.getEnglish());
    }

    public void setVocabularyStores(ArrayList<VocabularyStore> vocabularyStores) {
        this.vocabularyStores = vocabularyStores;
    }

    public int size() {
        return getVocabularyStores().size();
    }

    public boolean any() {
        return size() > 0;
    }
}
