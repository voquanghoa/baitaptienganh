package com.qh.learnenglish.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by osx on 4/1/17.
 */
public class WordData {
    @SerializedName("words")
    private List<WordItem> words;

    public List<WordItem> getWords() {
        return words;
    }

    public void setWords(List<WordItem> words) {
        this.words = words;
    }
}
