package com.qh.learnenglish.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by osx on 4/20/17.
 */
public class VocabularyStore extends Vocabulary{

    @SerializedName("category")
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
