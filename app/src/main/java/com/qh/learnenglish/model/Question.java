package com.qh.learnenglish.model;

import java.util.List;

/**
 * Created by osx on 3/9/17.
 */
public class Question {
    private String question;
    private List<String> answers;
    private int correct;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }
}
