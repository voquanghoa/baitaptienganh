package com.qh.learnenglish.model;

import java.util.ArrayList;

/**
 * Created by osx on 4/19/17.
 */
public class VocabularyCategoryList extends ArrayList<VocabularyCategory> {

    public VocabularyCategory getByFolder(String folder){
        for(VocabularyCategory category: this){
            if(category.getFolder().equals(folder)){
                return category;
            }
        }
        return null;
    }

}
