package com.qh.learnenglish;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.qh.learnenglish.adapter.LessonAdapter;
import com.qh.learnenglish.controller.DataController;
import com.qh.learnenglish.model.Lesson;

import java.io.IOException;

/**
 * Created by osx on 3/9/17.
 */
public class ExerciseActivity extends BaseActivity implements Runnable{

    private LessonAdapter lessonAdapter;
    private long startTime;
    private String fileDisplayName;
    private boolean isRunning;

    private static final int LESSON_TIME = 240;//4 minutes

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exercise_activity_layout);

        try {
            String fileName = getIntent().getStringExtra("file_path");
            fileDisplayName = getIntent().getStringExtra("file_name");
            lessonAdapter = new LessonAdapter(this, DataController.from(this).readJson(fileName + ".json", Lesson.class));
            ((ListView)findViewById(R.id.lesson)).setAdapter(lessonAdapter);
            isRunning = true;
            new Thread(this).start();
            startTime = System.currentTimeMillis();

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onSubmit(View view){
        view.setVisibility(View.GONE);
        finishLesson();
    }

    private void finishLesson(){
        isRunning = false;
        setTitle(fileDisplayName);

        String message = String.format(getString(R.string.score_message_format), lessonAdapter.getCorrectCount(), lessonAdapter.getCount());
        new AlertDialog.Builder(this)
                .setTitle(R.string.finish)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        lessonAdapter.setReviewMode(true);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void finish() {
        isRunning = false;
        super.finish();
    }


    @Override
    public void run() {
        while (isRunning){
            final int second = (int)(System.currentTimeMillis()-startTime)/1000;
            final int secondRemaining = LESSON_TIME - second;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTitle(String.format(getString(R.string.lesson_title_format), fileDisplayName, secondRemaining/60, secondRemaining%60));
                    if(secondRemaining<=0){
                        finishLesson();
                    }
                }
            });

            if(secondRemaining<=0){
                break;
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
